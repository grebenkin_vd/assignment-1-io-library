%define SYS_WRITE 1
%define FD_STDOUT 1
%define SYS_EXIT 60

section .text
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYS_EXIT ;запись кода системного вызова
    syscall ;system call
    ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax,rax ;i=0
    .loop: ;while(str[i]!=0) i++
        cmp byte[rax+rdi],0 ;если конец -> переход на .end
        je .end
        inc rax ;i++
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    ; WRIGHT
    ; %rax	arg0 (%rdi)	        arg1 (%rsi)	        arg2 (%rdx)
    ;  1    unsigned int fd     const char *buf     size_t count
    xor rax, rax
    push rdi
    call string_length
    pop rsi
    mov rdx, rax 
    mov rdi, FD_STDOUT 
    mov rax, SYS_WRITE 
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char: 
    ; WRIGHT
    ; %rax	arg0 (%rdi)	        arg1 (%rsi)	        arg2 (%rdx)
    ;  1    unsigned int fd     const char *buf     size_t count
    push rdi 
    mov rdx, 1     
    mov rsi, rsp 
    pop rdi
    mov rdi, FD_STDOUT 
    mov rax, SYS_WRITE      
    syscall         
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    mov rdi, '\n' 
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    ; WRIGHT
    ; %rax	arg0 (%rdi)	        arg1 (%rsi)	        arg2 (%rdx)
    ;  1    unsigned int fd     const char *buf     size_t count
    xor rax, rax 
    mov r8, 10
    mov r9, rsp
    mov rax, rdi 
    push 0 
    .loop:
        xor rdx,rdx
        div r8 
        add rdx, '0' 
        dec rsp 
        mov byte[rsp], dl 
        cmp rax,0
        je .end
        jmp .loop 

    .end:
        mov rdi, rsp 
        push r9
        call print_string
        pop r9
        mov rsp, r9
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    ; WRIGHT
    ; %rax	arg0 (%rdi)	        arg1 (%rsi)	        arg2 (%rdx)
    ;  1    unsigned int fd     const char *buf     size_t count
    xor rax, rax
    cmp rdi,0
    jl .neg 
    jmp print_uint

    .neg
        push rdi 
        mov rdi, '-'
        call print_char
        pop rdi
        neg rdi
        jmp print_uint 

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx 
    xor r8, r8 
    xor r9, r9
    .loop: 
        mov r8b, byte[rdi+rcx] 
        mov r9b, byte[rsi+rcx]
        cmp r8b,r9b 
        jne .no 
        cmp r8b,0
        je .yes 
        inc rcx 
        jmp .loop
    
    .yes:
        mov rax, 1   
        ret
    .no:
        mov rax,0 
        ret


read_char:
    xor rax, rax 
    xor rdi,rdi 
    mov rdx, 1 
    push 0 
    mov rsi, rsp 
    syscall
    pop rax 
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rcx, rcx
    xor rax, rax

    .loop:
        push rdi
        push rsi
        push rcx
        call read_char
        pop rcx
        pop rsi
        pop rdi
        cmp rax, 0
        je .end

        cmp rax, ' ' 
        je .skip_whitespace
        cmp rax, '	'
        je .skip_whitespace
        cmp rax, '\n'
        je .skip_whitespace

        mov [rdi+rcx], rax 
        inc rcx 
        cmp rcx, rsi 
        jge .error

        jmp .loop

    .skip_whitespace:
        cmp rcx,0
        je .loop
        jmp .end 
    .error: 
        xor rax, rax   
        xor rdx, rdx
        ret
    .end:
        xor rax, rax
        mov [rdi+rcx], rax 
        mov rax, rdi 
        mov rdx, rcx 
        ret
    




; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx
    mov r8, 10 
    .loop:
        movzx r9, byte[rdi+rcx] 
        cmp r9,0 
        je .end
        cmp r9b, '0' 
        jl .end    
        cmp r9b, '9'
        jg .end

        mul r8 
        sub r9b, '0' 
        add rax, r9 
        inc rcx 
        jmp .loop

    .end
        mov rdx, rcx 
        ret



parse_int:
    xor rax, rax
    xor rdx, rdx 
    mov rcx, rdi 
    cmp byte[rcx], '-'
    je .neg
    jmp .pos 
    .neg:
        inc rcx 
        mov rdi, rcx
        push rcx
        call parse_uint
        pop rcx
        neg rax 
        inc rdx 
        ret
    .pos:
        mov rdi, rcx
        jmp parse_uint 
        




string_copy: 
    xor rax, rax
    xor rcx, rcx 
    ;push rsi
    push rdi
    ;push rcx
    ;push rdx
    call string_length
    ;pop rdx
    ;pop rcx
    pop rdi
    ;pop rsi

    mov r8, rax 
    cmp rdx, r8 
    jl .error
    .loop:
        cmp rcx, r8
        jg .end
        mov r10,[rdi+rcx]
        mov [rsi+rcx], r10
        inc rcx
        jmp .loop
    
    .error:
        mov rax, 0 
        ret
    .end:
        mov rax, r8 
        ret      
